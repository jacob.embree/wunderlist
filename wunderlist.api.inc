<?php

/**
 * @file
 * Custom oauth2 class to connect to wunderlist.
 */

/**
 * {@inheritdoc}
 */
class Wunderlist extends OAuth2\Client {

  /**
   * Create a webhook for a list.
   *
   * Webhooks are triggered when a list is changed.
   *
   * @param int $list_id
   *   The list id to update.
   *
   * @return int
   *   The webhook id.
   */
  public function createWebhook($list_id) {
    global $base_url;

    $access_token = variable_get('wunderlist_token');
    $client_id = variable_get('wunderlist_client_id');
    $data = array(
      'list_id' => $list_id,
      'url' => $base_url . '/wunderlist/list/update/' . $list_id,
      'processor_type' => 'generic',
      'configuration' => '',
    );
    $url = variable_get('wunderlist_api_url', '') . '/webhooks';
    $headers = array(
      'X-Access-Token' => $access_token,
      'X-Client-ID' => $client_id,
    );
    $params = drupal_http_build_query($data);
    $response = drupal_http_request($url,
      array('headers' => $headers, 'method' => 'POST', 'data' => $params)
    );

    if ($response->code == 201) {
      $data = drupal_json_decode($response->data);
      return $data['id'];
    }
    else {
      $error = isset($response->error) ? $response->error : '';
      watchdog('wunderlist', 'Could not create webhook due to "%error"',
        array('%error' => $response->code . ' ' . $error));
    }
    return 0;
  }

  /**
   * Get webhooks for a list id.
   *
   * @param int $list_id
   *   The list id.
   *
   * @return array
   *   array with list ids.
   */
  public function getWebhooks($list_id) {
    $access_token = variable_get('wunderlist_token');
    $client_id = variable_get('wunderlist_client_id');
    $url = variable_get('wunderlist_api_url', '') . '/webhooks?list_id=' . $list_id;
    $headers = array(
      'X-Access-Token' => $access_token,
      'X-Client-ID' => $client_id,
    );
    $response = drupal_http_request($url,
      array('headers' => $headers, 'method' => 'GET')
    );
    if ($response->code == 200) {
      return drupal_json_decode($response->data);
    }
    return array();
  }

  /**
   * Deletes a webhook.
   *
   * @param int $revision_id
   *   The webhook id to delete.
   */
  public function deleteWebhook($revision_id) {
    $access_token = variable_get('wunderlist_token');
    $client_id = variable_get('wunderlist_client_id');
    $url = variable_get('wunderlist_api_url', '') . '/webhooks/' . $revision_id;
    $headers = array(
      'X-Access-Token' => $access_token,
      'X-Client-ID' => $client_id,
    );
    $response = drupal_http_request($url,
      array('headers' => $headers, 'method' => 'DELETE')
    );

    if ($response->code != 204) {
      $error = isset($response->error) ? $response->error : '';
      watchdog('wunderlist', 'Could not delete webhook due to "%error"',
        array('%error' => $response->code . ' ' . $error));
    }
  }

  /**
   * Returns a list from Wunderlist.
   *
   * @return array
   *   list array
   */
  public function getList() {
    $access_token = variable_get('wunderlist_token');
    $client_id = variable_get('wunderlist_client_id');
    $url = variable_get('wunderlist_api_url', '') . '/lists';
    $headers = array(
      'X-Access-Token' => $access_token,
      'X-Client-ID' => $client_id,
    );
    $response = drupal_http_request($url, array('headers' => $headers));

    if ($response->code == 200) {
      $data = drupal_json_decode($response->data);
    }
    else {
      $data = array();
      $error = isset($response->error) ? $response->error : '';
      watchdog('wunderlist', 'Could not fetch list due to "%error"',
        array('%error' => $response->code . ' ' . $error));
    }

    return $data;
  }

  /**
   * Return the task from one list item.
   *
   * @param int $id
   *   List id.
   *
   * @return array
   *   List array
   */
  public function getTasks($id) {
    $access_token = variable_get('wunderlist_token');
    $client_id = variable_get('wunderlist_client_id');
    $params = drupal_http_build_query(array('list_id' => $id));

    $url = variable_get('wunderlist_api_url', '') . '/tasks?' . $params;

    $headers = array(
      'X-Access-Token' => $access_token,
      'X-Client-ID' => $client_id,
      'Content-Type' => 'application/json',
    );

    $response = drupal_http_request($url, array('headers' => $headers));

    if ($response->code == 200) {
      $data = drupal_json_decode($response->data);
      foreach ($data as &$task) {
        $avatar = $this->getAvatar($task['created_by_id']);
        $task['avatar'] = $avatar;
      }
    }
    else {
      $data = array();
      watchdog('wunderlist', 'Could not fetch task due to "%error"',
        array(
          '%error' => $response->code . ' ' . $response->error . ': ' . $response->data,
        ));
    }

    return $data;
  }

  /**
   * Get the avatar of the user.
   *
   * @param int $user_id
   *   The id from the user.
   * @param int $size
   *   The size of the image.
   *
   * @return string
   *   The base64 encoded image.
   */
  public function getAvatar($user_id = NULL, $size = 64) {
    $access_token = variable_get('wunderlist_token');
    $client_id = variable_get('wunderlist_client_id');

    if (empty($user_id)) {
      $user = $this->getUser();
      $user_id = $user['id'];
    }

    $params = drupal_http_build_query(array('user_id' => $user_id, 'size' => $size));
    $url = variable_get('wunderlist_api_url', '') . '/avatar?' . $params;

    $headers = array(
      'X-Access-Token' => $access_token,
      'X-Client-ID' => $client_id,
    );

    $response = drupal_http_request($url, array(
      'headers' => $headers,
      'max_redirects' => 3,
    ));

    if ($response->code == 200) {
      $image = base64_encode($response->data);
    }
    else {
      $image = '';
      watchdog('wunderlist', 'Could not fetch image due to "%error"',
        array('%error' => $response->code . ' ' . $response->error));
    }

    return $image;
  }

  /**
   * Get authenticated Wunderlist user.
   *
   * @return array
   *   user array
   */
  public function getUser() {
    $access_token = variable_get('wunderlist_token');
    $client_id = variable_get('wunderlist_client_id');

    $url = variable_get('wunderlist_api_url', '') . '/user';

    $headers = array(
      'X-Access-Token' => $access_token,
      'X-Client-ID' => $client_id,
    );

    $response = drupal_http_request($url, array('headers' => $headers));

    if ($response->code == 200) {
      $data = drupal_json_decode($response->data);
      variable_set('wunderlist_user', $data);
    }
    else {
      $data = NULL;
      watchdog('wunderlist', 'Could not fetch image due to "%error"',
        array('%error' => $response->code . ' ' . $response->error));
    }

    return $data;
  }

}
